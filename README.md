# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | Y         |


---

### How to use 

    Describe how to use your web and maybe insert images to help you explain.
* The overall image
    ![](https://i.imgur.com/iWxWjti.jpg)
    
* Describing the function
    * ![](https://i.imgur.com/UduUjqJ.png)pen
        * Draw whatever you want in the canvas
            ![](https://i.imgur.com/1NWT35C.png)
        * change the width of the brush.
            ![](https://i.imgur.com/b09vuLR.png)
            ![](https://i.imgur.com/LL6unTs.png)
    * ![](https://i.imgur.com/6h33GpS.png)eraser 
        * erase the thing you draw wrong.
            ![](https://i.imgur.com/RnMOjvI.png)\
        * can also change the width of the eraser.
            ![](https://i.imgur.com/oxjxxa4.png)

    * ![](https://i.imgur.com/9svGZ0w.png)text
        * text the word into the canvas.
            ![](https://i.imgur.com/ETVGG9K.png)
        * change the size and the sytle of the font.
            ![](https://i.imgur.com/cKXhXgu.png)
    * ![](https://i.imgur.com/MxUVcYR.png) undo
        * return to the last step.
            ![](https://i.imgur.com/uLCQfLT.png)
    * ![](https://i.imgur.com/SbZP87k.png) redo
        * go to the next step
            ![](https://i.imgur.com/cKXhXgu.png)
    * ![](https://i.imgur.com/zbYKCI0.png) refresh
        * clean the canvas.
            ![](https://i.imgur.com/YfFWx1d.png)
    * ![](https://i.imgur.com/RhdJo5c.png)![](https://i.imgur.com/mFcciAN.png)![](https://i.imgur.com/pMGUYFu.png) rectangle, circle, and triangle.
        * draw the shape of rectangle, circle and triangle.
            ![](https://i.imgur.com/eFoLMJk.png)
    * ![](https://i.imgur.com/qs5yJ1S.png) upload
        * upload the file to the canvas
            ![](https://i.imgur.com/Drf6BNs.png)
        * get the photo of my friends Allen and resize the canvas.
            ![](https://i.imgur.com/bcqddSs.jpg)
    * ![](https://i.imgur.com/2OJWE6u.png) download
        * the default file name is **canvas.png**.
            ![](https://i.imgur.com/nnFtHkM.png)
        * after saving the file, we can get the canvas.png.\
            ![](https://i.imgur.com/9ZaMPFu.png)


### Function description

    Decribe your bouns function and how to use it.
* ![](https://i.imgur.com/QoAdmrq.png) rainbow brush
    * draw a beeeautiful color!
        ![](https://i.imgur.com/SdDJK3t.png)


### Gitlab page link

    your web page URL, which should be "https://[studentID].gitlab.io/AS_01_WebCanvas"
    
https://108062219.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    Anythinh you want to say to TAs.
I haven't figure out what I want to say to TAs yet.
Oh! TAs are all handsome and beutiful!



<style>
table th{
    width: 100%;
}
</style>