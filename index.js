const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

var back = document.getElementById('undo');
var load  = document.getElementById('load');
var save  = document.getElementById('save');
var clear = document.getElementById('refresh');
var redo = document.getElementById("redo");
var download = document.getElementById('download');
var tmpImgData;
var state;

//save state
function saveState(){
    state = ctx.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);
}

//initial position
let [lastX, lastY] = [0, 0];
//announce whether the mouse is clicking
let isDrawing = false;
let isDragging = false;
let beginX = 0;
let beginY = 0;
let hue = 0;
//by default create a image.
saveState();
//initialize the text
let hasInput = false;


download.addEventListener('click', function(){
    //create a ahref
    var link = document.createElement('a');
    link.download = 'canvas.png';
    link.href = canvas.toDataURL();
    link.click();
});

// undo
back.addEventListener('click', function(){
    window.history.go(-1);
}, false);

//clear
clear.addEventListener('click', function(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    saveState();
});
//redo
redo.addEventListener('click', function(){
    window.history.go(+1);
}, false);


canvas.addEventListener("mousedown", function(e){
    isDrawing = true;
    [lastX, lastY] = [e.offsetX, e.offsetY];
    if(!isDragging){
        tmpImgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        ctx.beginPath();
        beginX = e.offsetX;
        beginY = e.offsetY;
        isDragging = true;
    }
    return false;
}, false)
canvas.addEventListener("mouseup", stopDrawing);
canvas.addEventListener("mousemove", draw);

//pop the state.
window.addEventListener("popstate", function(e){
    //clean the canvas
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    if(e.state){
        ctx.putImageData(e.state, 0, 0);
    }
}, false);



function initialdraw(){
    //the setup of the painter
    ctx.lineJoin = "round";
    ctx.lineCap = "round";
    //color picker hsl, hue: 0-360, Saturation: 100%: full color, Ligntness: 0:white
    if(mode=="rainbow") {ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`}
    else {ctx.strokeStyle = document.getElementById("color-picker").value;}
    ctx.lineWidth = document.getElementById("brush-size").value;
}

//tempory save the image and load from it
function loadImage(){
    ctx.putImageData(tmpImgData, 0, 0);
}
//draw when the mouse moving
function draw(e){
    if(!isDrawing)return;
    initialdraw();
    ctx.beginPath();
    if(mode=="pen"){
        //the setup of the path
        ctx.globalCompositeOperation = "source-over";
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();
    }else if(mode=="eraser"){
        //the setup of the path
        ctx.globalCompositeOperation="destination-out";
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();
    }else if(mode=="rectangle"){
        loadImage();
        ctx.globalCompositeOperation = "source-over";
        ctx.strokeRect(beginX, beginY, e.offsetX - beginX, e.offsetY - beginY);
    }else if(mode=="circle"){
        ctx.globalCompositeOperation = "source-over";
        loadImage();
        var radius = Math.sqrt((e.offsetX-beginX)*(e.offsetX-beginX) + (e.offsetY-beginY)*(e.offsetY-beginY))/2;
        ctx.arc((beginX+e.offsetX)/2, (beginY+e.offsetY)/2, radius, 0, 2*Math.PI);
        ctx.stroke();
    }else if(mode=="triangle"){
        //draw the triangle
        ctx.globalCompositeOperation = "source-over";
        loadImage();
        ctx.moveTo(beginX, e.offsetY);
        ctx.lineTo((e.offsetX+beginX)/2, beginY);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.closePath();
        ctx.stroke();
    }else if(mode=="rainbow"){
        hue++;
        if(hue >= 360){hue = 0;}
        ctx.globalCompositeOperation = "source-over";
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();
    }
    [lastX, lastY] = [e.offsetX, e.offsetY];
}

//when stop drawing, the window save the state.
function stopDrawing(e){
    if(mode=="text"){
        ctx.globalCompositeOperation = "source-over";
        if(hasInput)return;
        addInput(e.offsetX, e.offsetY);
    }
    else{
        saveState();
    }
    isDragging = false;
    isDrawing = false;
    
}


function addInput(x, y) {
    var input = document.createElement('input');
    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = x + 'px';
    input.style.top = y + 'px';
    input.onkeydown = handleEnter;
    document.body.appendChild(input);
    input.focus();
    hasInput = true;
}

//Key handler for input box:
function handleEnter(e) {
    if (e.key == "Enter") {
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        hasInput = false;
    }
}

//Draw the text onto canvas:
function drawText(txt, x, y) {
    var fontsize_select = document.getElementById("text-fontsize").value;
    var fontstyle_select = document.getElementById("text-fonttype").value;
    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';
    ctx.font = fontsize_select + 'px ' + fontstyle_select;
    ctx.fillStyle = document.getElementById("color-picker").value;
    console.log(txt, x, y, ctx.font );
    ctx.fillText(txt, x, y);

    //save the state when finish texting
    saveState();
}

//upload the image and paste it
var imgLoader = document.getElementById("imageLoader");
imgLoader.addEventListener('change', handleImage, false);

function handleImage(e){
    if(!e.target.files)return
    let imageFile = e.target.files[0] // here we get the image file
    var reader = new FileReader();
    reader.readAsDataURL(imageFile);//read the data to url
    reader.onload = function(event){
        //converted image is available here.
        var img = new Image();
        img.src = event.target.result;//assigns converted image to image object
        //img.onload: set the operation after the image is loaded
        img.onload = function(ev){
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img, 0, 0);
        }
    }
}

//change_mode
var last_id = "pen";
canvas.classList.add("pen");
mode = "pen";
function changeMode(select_id){ 
    canvas.classList.remove(last_id);
    canvas.classList.add(select_id);
    mode = select_id;
    last_id = select_id;
}




